import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.linear_model import LogisticRegression
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score, ConfusionMatrixDisplay

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

#a
cmap = ListedColormap(['red', 'blue'])
plt.scatter(X_train[:, 0], X_train[:, 1], c = y_train, cmap=cmap)
plt.scatter(X_test[:, 0], X_test[:, 1], c = y_test, cmap=cmap, marker='x')
plt.legend(['Train data', 'Test data'])
plt.show()

#b
LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train, y_train)

#c
b = LogRegression_model.intercept_[0]
w1, w2 = LogRegression_model.coef_.T
c = -b / w2
m = -w1 / w2
xmin, xmax = -4, 4
ymin, ymax = -4, 4
xd = np.array([xmin, xmax])
yd = m * xd + c
plt.scatter(x=X_train[:, 0], y=X_train[:, 1], c=y_train, cmap=cmap, label='Train')
plt.plot(xd, yd, 'k', lw=1, ls='--')
plt.fill_between(xd, yd, ymin, color='tab:blue', alpha=0.2)
plt.fill_between(xd, yd, ymax, color='tab:red', alpha=0.2)
plt.xlim(xmin, xmax)
plt.ylim(ymin, ymax)
plt.show()

#d
y_test_p = LogRegression_model.predict(X_test)
cm = confusion_matrix(y_test, y_test_p)
disp = ConfusionMatrixDisplay(cm)
disp.plot()
plt.show()
print(f'Accuracy: {accuracy_score(y_test, y_test_p)}')
print(f'Precision: {precision_score(y_test, y_test_p)}')
print(f'Recall: {recall_score(y_test, y_test_p)}')
print(f'F1 score: {f1_score(y_test, y_test_p)}')

#e
boolArray = y_test == y_test_p
cmap = ListedColormap(['black', 'green'])
plt.scatter(x=X_test[:, 0], y=X_test[:, 1], c=boolArray, cmap=cmap)
plt.colorbar(label='False/True', ticks=np.linspace(0, 1, 2))
plt.show()