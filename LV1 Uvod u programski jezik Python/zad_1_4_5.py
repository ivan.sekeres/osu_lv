file = open("SMSSpamCollection.txt")

count_ham = 0
count_spam = 0
avg_ham = 0
avg_spam = 0
q_mark_count = 0

for line in file:
    line = line.rstrip()
    if(line.startswith("ham")):
        words = line[1:].split()
        avg_ham += len(words)
        count_ham += 1
    elif(line.startswith("spam")):
        words = line[1:].split()
        avg_spam += len(words)
        count_spam += 1
        if(line.endswith("!")):
            q_mark_count += 1
file.close()

avg_ham /= count_ham
avg_spam /= count_spam

print(f"Prosjecan broj rijeci u porukama tipa ham: {avg_ham}")
print(f"Prosjecan broj rijeci u porukama tipa spam: {avg_spam}")
print(f"Broj poruka koje zavrsavaju usklicnikom: {q_mark_count}")