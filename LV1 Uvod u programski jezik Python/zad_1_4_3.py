numbers = []

while True:
    str_input = input("Unesi broj: ")
    if str_input == "Done": 
        break
    try:     
        number = int(str_input)
        numbers.append(number)
    except:
        print("Krivi unos.")

avg = 0
for number in numbers:
    avg += number
avg /= len(numbers)
minimum = min(numbers)
maximum = max(numbers)

numbers.sort()

print(f"Broj brojeva: {len(numbers)}")
print(f"Avg: {str(avg)}")
print(f"Min: {str(minimum)}")
print(f"Max: {str(maximum)}")
print(f"List: {str(numbers)}")