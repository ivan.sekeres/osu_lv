try:
    grade = float(input("Unesi ocjenu: "))
    if grade >= 0 or grade <= 1.0:
        if grade >= 0.9:
           print("Grade: A")
        elif grade >= 0.8:
           print("Grade: B")
        elif grade >= 0.7:
           print("Grade: C")
        elif grade >= 0.6:
           print("Grade: D")
        else:
           print("Grade: F")
    else:
       print("Ocjena nije unutar zadanog intervala.")
except:
  print("Krivi unos.")