import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

# model = keras.models.load_model("LV8 Umjetne neuronske mreže/FCN/")
model = keras.models.load_model('model/')

image = keras.utils.load_img(
    'LV8 Umjetne neuronske mreže\\4.png', color_mode="grayscale", target_size=(28, 28))

image_array = np.array(image)
image_array = image_array.astype("float32") / 255
image_array = np.expand_dims(image_array, -1)
image_array = np.reshape(image_array, (1, 784))

prediction = model.predict(image_array)

print(prediction)
prediction_class = np.argmax(prediction)
print("Predicted: ", prediction_class)

plt.imshow(image, cmap='gray')
plt.title(f"pred: {prediction_class}")
plt.show()
