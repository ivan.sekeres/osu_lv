import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score, max_error
from math import sqrt

data = pd.read_csv('data_C02_emission.csv')

ohe = OneHotEncoder()

output_variables = ['CO2 Emissions (g/km)']
input_variables = ['Engine Size (L)',
          'Cylinders',
          'Fuel Type',
          'Fuel Consumption City (L/100km)',
          'Fuel Consumption Hwy (L/100km)',
          'Fuel Consumption Comb (L/100km)',
          'Fuel Consumption Comb (mpg)']

X_encoded = ohe.fit_transform(data[['Fuel Type']]).toarray()

data['Fuel Type'] = X_encoded

X = data[input_variables].to_numpy()
y = data[output_variables].to_numpy()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

linearModel = lm.LinearRegression()
linearModel.fit(X_train, y_train)
print(linearModel.coef_)

y_test_p = linearModel.predict(X_test)

print(f'Mean squared error: {mean_squared_error(y_test, y_test_p)}')
print(f'Root mean squared error: {sqrt(mean_squared_error(y_test, y_test_p))}')
print(f'Mean absolute error: {mean_absolute_error(y_test, y_test_p)}')
print(f'Mean absolute percentage error: {mean_absolute_percentage_error(y_test, y_test_p)}')
print(f'R2 score: {r2_score(y_test, y_test_p)}')

error = (y_test_p - y_test)
max_error_id = np.argmax(error)

max_error_model = data.iloc[max_error_id]

print(f"Max error model:\n{max_error_model}")