import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score
from math import sqrt

data = pd.read_csv('data_C02_emission.csv')

#a
y = data['CO2 Emissions (g/km)']
X = data[['Engine Size (L)',
          'Cylinders',
          'Fuel Consumption City (L/100km)',
          'Fuel Consumption Hwy (L/100km)',
          'Fuel Consumption Comb (L/100km)',
          'Fuel Consumption Comb (mpg)']]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

#b
plt.scatter(x=X_train['Engine Size (L)'], y=y_train, s=2, c='blue')
plt.scatter(x=X_test['Engine Size (L)'], y=y_test, s=2, c='red')
plt.ylabel('CO2 Emissions (g/km)')
plt.xlabel('Engine Size (L)')
plt.legend(['Train', 'Test'])
plt.show()

#c
sc = MinMaxScaler()
X_train_n = pd.DataFrame(sc.fit_transform(X_train), columns=X_train.columns)
X_test_n = pd.DataFrame(sc.transform(X_test), columns=X_test.columns)

plt.subplot(2,1,1)
plt.hist(X_train['Engine Size (L)'])
plt.title('Before scaling', fontsize=10)
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
plt.subplot(2,1,2)
plt.hist(X_train_n['Engine Size (L)'])
plt.title('After scaling', fontsize=10)
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

#d
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)

#e
y_test_p = linearModel.predict(X_test_n)

plt.scatter(x=y_test, y=y_test_p, s=2, c='blue')
plt.xlabel('Real data')
plt.ylabel('Predict data')
"""
plt.scatter(x=X_test_n['Engine Size (L)'], y=y_test, s=2, c='blue')
plt.scatter(x=X_test_n['Engine Size (L)'], y=y_test_p, s=2, c='red')
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
"""
plt.show()

#f
print(f'Mean squared error: {mean_squared_error(y_test, y_test_p)}')
print(f'Root mean squared error: {sqrt(mean_squared_error(y_test, y_test_p))}')
print(f'Mean absolute error: {mean_absolute_error(y_test, y_test_p)}')
print(f'Mean absolute percentage error: {mean_absolute_percentage_error(y_test, y_test_p)}')
print(f'R2 score: {r2_score(y_test, y_test_p)}')
