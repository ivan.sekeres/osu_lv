import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs/test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Original image")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

print(f'Number of colors in image: {len(np.unique(img_array_aprox, axis=0))}')

km = KMeans(n_clusters=5, init='random', n_init=5)
km.fit(img_array_aprox)
colorLabels = km.predict(img_array_aprox)
quantizedColors = km.cluster_centers_[colorLabels]
quantizedImg = np.reshape(quantizedColors, (w, h, d))
plt.figure()
plt.title("Quantized image")
plt.imshow(quantizedImg)
plt.tight_layout()
plt.show()

j = []
for n_clusters in range(1, 10):  
    km = KMeans(n_clusters=n_clusters, init='random', n_init=5)
    km.fit(img_array_aprox)
    colorLabels = km.predict(img_array_aprox)
    j.append(km.inertia_)

plt.plot(range(1, 10), j)
plt.xlabel('K')
plt.ylabel('J')
plt.show()

for i in range(0, 4):
    boolArray = colorLabels == i
    boolArray = np.reshape(boolArray, (w, h))
    plt.subplot(1, 4, i + 1)
    plt.title(f"Binary image of {i + 1}. group")
    plt.imshow(boolArray)
    
plt.show()
