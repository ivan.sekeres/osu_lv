import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('LV2 Rad s bibliotekama Numpy i Matplotlib\data.csv', delimiter=',')

print(data)

#a
print("Mjeranja su izvrsena na " + str(len(data)) + " osoba.")

#b
plt.scatter(data[:, 1], data[:, 2], s=0.5)
plt.xlabel('visina')
plt.ylabel('masa')
plt.show()

#c
plt.scatter(data[::50, 1], data[::50, 2], s=0.5)
plt.xlabel('visina')
plt.ylabel('masa')
plt.show()

#d
print("Min: " + str(data[:, 1].min()))
print("Max: " + str(data[:, 1].max()))
print("Avg: " + str(data[:, 1].mean()))

#e
m = (data[:, 0] == 1)
z = (data[:, 0] == 0)
print("Min m: " + str(data[m, 1].min()))
print("Max m: " + str(data[m, 1].max()))
print("Avg m: " + str(data[m, 1].mean()))
print("Min z: " + str(data[z, 1].min()))
print("Max z: " + str(data[z, 1].max()))
print("Avg z: " + str(data[z, 1].mean()))