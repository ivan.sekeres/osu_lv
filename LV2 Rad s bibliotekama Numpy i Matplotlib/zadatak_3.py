import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("LV2 Rad s bibliotekama Numpy i Matplotlib\\road.jpg")
img = img[ :,:,0].copy()
plt.figure()
plt.imshow(img, cmap="gray")

#a
light_img = img[:,:] 
plt.figure()
plt.imshow(light_img, cmap="gray", alpha=0.5)

#b
one_fourth_img = img[:, round(len(img[:, 0])/3*1):round(len(img[:, 0])/3*2)] 
plt.figure()
plt.imshow(one_fourth_img, cmap="gray")

#c
rotated_img = np.rot90(img, k=-1)
plt.figure()
plt.imshow(rotated_img, cmap="gray")

#d
mirrored_img = np.flip(img, axis=1)
plt.figure()
plt.imshow(mirrored_img, cmap="gray")

plt.show()