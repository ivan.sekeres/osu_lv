import numpy as np
import matplotlib.pyplot as plt

z = np.zeros((50, 50))
o = np.ones((50, 50))

img = np.vstack((np.hstack((z ,o)), np.hstack((o, z))))
plt.figure()
plt.imshow(img , cmap ="gray")
plt.show()