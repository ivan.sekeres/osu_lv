import pandas as pd
import matplotlib.pyplot as plt

#data = pd.read_csv('LV3 Upoznavanje s Pandas bibliotekom. Predobrada podataka i eksplorativna analiza podataka\data_C02_emission.csv')
data = pd.read_csv('data_C02_emission.csv')

#a
plt.figure()
data['CO2 Emissions (g/km)'].plot(kind='hist', bins=50)

#b
b = data.copy()
b.loc[b['Fuel Type'] == 'X', 'Fuel Type'] = 0
b.loc[b['Fuel Type'] == 'Z', 'Fuel Type'] = 1
b.loc[b['Fuel Type'] == 'D', 'Fuel Type'] = 2
b.loc[b['Fuel Type'] == 'E', 'Fuel Type'] = 3
b.loc[b['Fuel Type'] == 'N', 'Fuel Type'] = 4

b.plot.scatter(x='Fuel Consumption City (L/100km)',
                  y='CO2 Emissions (g/km)', c=b['Fuel Type'])

#c
c = data.groupby('Fuel Type')
c.boxplot(column=['Fuel Consumption Hwy (L/100km)'])

#d
plt.figure()
d = data.groupby('Fuel Type')['Fuel Type'].count()
plt.ylabel('Number of cars')
d.plot.bar('Fuel Type')

#e  Pomocu stupcastog grafa prikažite na istoj slici prosjecnu C02 emisiju vozila s obzirom na broj cilindara.
plt.figure()
e = data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
plt.ylabel('CO2 Emissions (g/km)')
e.plot.bar()

plt.show()