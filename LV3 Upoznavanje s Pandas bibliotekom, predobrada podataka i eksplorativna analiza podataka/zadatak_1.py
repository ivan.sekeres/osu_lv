import pandas as pd

data = pd.read_csv('LV3 Upoznavanje s Pandas bibliotekom. Predobrada podataka i eksplorativna analiza podataka\data_C02_emission.csv')

#a
length = len(data)
print(f"Number of data: {length}")

print("Type of data:")
print(data.dtypes)

print("Dropping duplicated data...")
data.drop_duplicates()
data = data.reset_index(drop=True)
print(f"Number of data: {len(data)}")
print(f"Number of deleted data: {length - len(data)}")
data = data.reset_index(drop=True)

print(data.info())
print(data.describe())
print(data.max())
print(data.min())

#b
print("\n\n\n\n\n")
data = data.sort_values(by=['Fuel Consumption City (L/100km)'])
print("Cars with lowest city fuel consumption: ")
print(data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3))
print("Cars with highest city fuel consumption: ")
print(data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].tail(3))

#c
print("\n\n\n\n\n")
c = data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
print(f"Number of vehicles: {len(c)}")
print(c)
print(f"Average CO2 emission: {c['CO2 Emissions (g/km)'].mean()} g/km")

#d
print("\n\n\n\n\n")
d = data[(data['Make'] == 'Audi')]
print(f"Number of vehicles: {len(d)}")
d = d[(d['Cylinders'] == 4)]
print(f"Number of vehicles: {len(d)}")
print(f"Average CO2 emission: {d['CO2 Emissions (g/km)'].mean()} g/km")

#e
print("\n\n\n\n\n")
e3 = data[(data['Cylinders'] == 3)]
e4 = data[(data['Cylinders'] == 4)]
e5 = data[(data['Cylinders'] == 5)]
e6 = data[(data['Cylinders'] == 6)]
e8 = data[(data['Cylinders'] == 8)]
e10 = data[(data['Cylinders'] == 10)]
e12 = data[(data['Cylinders'] == 12)]
e16 = data[(data['Cylinders'] == 16)]
print(f"Number of vehicles with 3 cylinders {len(e3)},average CO2 emmision {e3['CO2 Emissions (g/km)'].mean()} g/km")
print(f"Number of vehicles with 4 cylinders {len(e4)},average CO2 emmision {e4['CO2 Emissions (g/km)'].mean()} g/km")
print(f"Number of vehicles with 5 cylinders {len(e5)},average CO2 emmision {e5['CO2 Emissions (g/km)'].mean()} g/km")
print(f"Number of vehicles with 6 cylinders {len(e6)},average CO2 emmision {e6['CO2 Emissions (g/km)'].mean()} g/km")
print(f"Number of vehicles with 8 cylinders {len(e8)},average CO2 emmision {e8['CO2 Emissions (g/km)'].mean()} g/km")
print(f"Number of vehicles with 10 cylinders {len(e10)},average CO2 emmision {e10['CO2 Emissions (g/km)'].mean()} g/km")
print(f"Number of vehicles with 12 cylinders {len(e12)},average CO2 emmision {e12['CO2 Emissions (g/km)'].mean()} g/km")
print(f"Number of vehicles with 16 cylinders {len(e16)},average CO2 emmision {e16['CO2 Emissions (g/km)'].mean()} g/km")

#f
print("\n\n\n\n\n")
fd = data[(data['Fuel Type'] == 'D')]
frg = data[(data['Fuel Type'] == 'X')]
print(f"Average city fuel consumption for diesel cars: {fd['Fuel Consumption City (L/100km)'].mean()}")
print(fd['Fuel Consumption City (L/100km)'].describe())
print(f"\nAverage city fuel consumption for regular gasoline cars: {frg['Fuel Consumption City (L/100km)'].mean()}")
print(frg['Fuel Consumption City (L/100km)'].describe())

#g
print("\n\n\n\n\nadasdasdasdasdfasfcafasdfasddfasd")
g = data[(data['Cylinders'] == 4) & (data['Fuel Type'] == 'D')]
g = g[g['Fuel Consumption City (L/100km)'] == g['Fuel Consumption City (L/100km)'].max()]
print(g[['Make', 'Model', 'Fuel Consumption City (L/100km)']])

#h
print("\n\n\n\n\n")
h = data[(data['Transmission'].str[0] == 'M')]
print(f"Number of cars with manual transmission: {len(h)}")

#i
print("\n\n\n\n\n")
print(data.corr(numeric_only=True))
